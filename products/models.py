from django.db import models
from budgets.models import Budget
from ecommerces.models import Ecommerce

class Product(models.Model):
    name  = models.CharField(max_length=250) 
    url = models.CharField(max_length=2500)
    budget = models.ForeignKey(Budget, related_name='products', on_delete=models.CASCADE, null=True)
    ecommerce = models.ForeignKey(Ecommerce, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name
