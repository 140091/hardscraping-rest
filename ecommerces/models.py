from django.db import models

class Ecommerce(models.Model):

    PARAM_TYPES = (
        ('XP', 'XPATH'),
        ('CS', 'CSS SELECTOR'),
    )
    
    name = models.CharField(max_length=250)
    product_list_url = models.CharField(max_length=2000)
    # logo = models.ImageField()
    
    name_param = models.CharField(max_length=300)
    name_param_type = models.CharField(max_length=2, choices=PARAM_TYPES)

    def __str__(self):
        return self.name
