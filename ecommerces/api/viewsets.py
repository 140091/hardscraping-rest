from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ecommerces.models import Ecommerce
from .serializers import EcommerceSerializer

class EcommerceViewSet(ModelViewSet):
    queryset = Ecommerce.objects.all()
    serializer_class = EcommerceSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated, IsAdminUser,)
