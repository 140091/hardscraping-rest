from rest_framework.serializers import ModelSerializer
from ecommerces.models import Ecommerce

class EcommerceSerializer(ModelSerializer):
    class Meta:
        model = Ecommerce
        fields = ['id', 'name', 'product_list_url', 'name_param', 'name_param_type']
