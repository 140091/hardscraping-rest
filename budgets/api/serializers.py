from rest_framework.serializers import ModelSerializer
from products.api.serializers import ProductSerializer
from budgets.models import Budget

class BudgetSerializer(ModelSerializer):
    products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Budget
        fields = ['id', 'name', 'user', 'products']
